package test_opencv;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.Font;

public class Fenetre2 extends JFrame  {

	private JPanel contentPane;

	
	/**
	 * Create the frame.
	 */
	public Fenetre2() {
		// la fenetre qui affiche le message help avec un petit descriptif sur comment utiliser l'application
		setTitle("Help");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(1020, 310, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTextPane txtpnVampsdetectPermetDe = new JTextPane();
		txtpnVampsdetectPermetDe.setFont(new Font("Century Schoolbook", Font.BOLD, 17));
		txtpnVampsdetectPermetDe.setText("VampsDetect permet de recona\u00EEtre des panneaux de signalisation et d\u2019afficher un message de limitation accompagn\u00E9 du panneau de r\u00E9f\u00E9rence associ\u00E9.\r\n\r\nDeux options sont possibles : \r\n1.  Lire une vid\u00E9o \u00E0 partir d\u2019un fichier dans le r\u00E9pertoire.\r\n2.  Lancer la webcam et d\u00E9tecter les panneaux apparaissant sur un t\u00E9l\u00E9phone.");
		contentPane.add(txtpnVampsdetectPermetDe, BorderLayout.CENTER);
		
	}

}
