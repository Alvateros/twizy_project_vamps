package test_opencv;

import java.util.List;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.highgui.Highgui; // pour la partie lecture d'image
import org.opencv.imgproc.Imgproc;
import org.w3c.dom.css.Rect;


public class Utile extends Fenetre {
	public static final int LARGEUR_STANDARD=254,LONGUEUR_STANDARD=254;
    public static final Size FORMAT_STANDARD= new Size(LONGUEUR_STANDARD, LARGEUR_STANDARD);


    

	//m�thode pour lire une image
		public static Mat LectureImage(String fichier){
			File f=new File(fichier);
			Mat m =Highgui.imread(f.getAbsolutePath());
			return m;
		}
		
		//m�thode pour afficher les canaux couleur d'une image (HSV)
		public static void ImShow(String title, Mat img){
			MatOfByte MatOfByte=new MatOfByte();
			Highgui.imencode(".jpg",img,MatOfByte);
			byte[] byteArray = MatOfByte.toArray();
			BufferedImage bufImage = null;
			try {
				InputStream in= new ByteArrayInputStream(byteArray);
				bufImage=ImageIO.read(in);
				JFrame frame=new JFrame();
				frame.setTitle(title);
				frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
				frame.pack();
				frame.setVisible(true);
			}catch (Exception e){
				e.printStackTrace();
			}
		}

		//m�thode qui affiche dans un cadre noir les objet de couleur choisie
		//en blanc ( le filtre Gaussian utilis� sert � lisser l'image)
		public static Mat Seuillage (Mat hsv_image){
				Mat threshold_img1 = new Mat();
				Mat threshold_img2 = new Mat();
				Mat threshold_img = new Mat();
				// changer les num�ri dans les deux lignes suivantes selon la couleur d�sir�e
				Core.inRange(hsv_image,  new Scalar(0,10,10), new Scalar(12,255,255), threshold_img1);
				Core.inRange(hsv_image,  new Scalar(168,0,0), new Scalar(180,255,255), threshold_img2);			
				Core.bitwise_or(threshold_img1,threshold_img2, threshold_img);
				Imgproc.GaussianBlur(threshold_img, threshold_img, new Size(9,9), 2,2);
				return threshold_img;
				
			}
		
		// Detecter Cercle ( cette m�thode d�ssine les contours sur une image noir)
		//  Elle d�ssine que les contours rends
		public static Mat DetecterCercles (Mat img){
				int thresh=100;
				Mat canny_output = new Mat();
				ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
				MatOfInt4 hierarchy=new MatOfInt4();
				Imgproc.Canny(img, canny_output, thresh, thresh*2);
				Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
				MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
				float[] radius = new float[1];
				Point center = new Point();
				for (int c=0;c<contours.size();c++){
						MatOfPoint contour=contours.get(c);
						double contourArea=Imgproc.contourArea(contour);
						matOfPoint2f.fromList(contour.toList());
						Imgproc.minEnclosingCircle(matOfPoint2f, center,  radius);
						if((contourArea/(Math.PI*radius[0]*radius[0]))>=0.97){
							Core.circle(drawing, center, (int)radius[0],new Scalar(0,255,0), 2);	
						}
				}
				return drawing;
		}
		
		//d�ssiner contour cercle rouge sur l'image d'origine
		// m�me principe que la m�thode "DetecterCercles" mais les contour
		//sont d�ssin� en vert sur l'image do'irigne
		public static Mat Contours_sur_origine (Mat img, Mat img2){
		
			int thresh=100;
			Mat canny_output = new Mat();
			ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			MatOfInt4 hierarchy=new MatOfInt4();
			Imgproc.Canny(img, canny_output, thresh, thresh*2);
			Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
			Random rand = new Random();
			MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
			float[] radius = new float[1];
			Point center = new Point();
			for (int c=0;c<contours.size();c++){
					MatOfPoint contour=contours.get(c);
					double contourArea=Imgproc.contourArea(contour);
					matOfPoint2f.fromList(contour.toList());
					Imgproc.minEnclosingCircle(matOfPoint2f, center,  radius);
					// on choisis la taille des cercles que l'on accepte de tracer
					if((contourArea/(Math.PI*radius[0]*radius[0]))>=0.8){
						Core.circle(img2, center, (int)radius[0],new Scalar(0,255,0), 2);	
					}
			}
			return img2;
	}
		
		//Detection de contour quleconque
		//m�me principe que la detection des cercles 
		//ici on detecte les contour de toutes les formes
		public static Mat DetecterContour (Mat img){
			int thresh=100;
			Mat canny_output = new Mat();
			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			MatOfInt4 hierarchy=new MatOfInt4();
			Imgproc.Canny(img, canny_output, thresh, thresh*2);
			Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
			Random rand = new Random();
			for (int i=0;i<contours.size();i++){
				Scalar color = new Scalar ( rand.nextInt(255-0+1), rand.nextInt(255-0+1), rand.nextInt(255-0+1));
				Imgproc.drawContours(drawing, contours, i, color, 1,8,hierarchy,0,new Point());
			}
			return drawing;
			
		}

		//matching avec mise � l'�chelle et extraction des caract�ristiques
		public static Mat Matchingg (Mat sroadSign, Mat object){
			//Misa � echelle
			Mat sObject = new Mat();
			Imgproc.resize(object,  sObject,  sroadSign.size());
			Mat grayObject = new Mat(sObject.rows(), sObject.cols(), sObject.type());
			Imgproc.cvtColor(sObject, grayObject, Imgproc.COLOR_BGRA2GRAY);
			Core.normalize(sObject, grayObject, 0, 255, Core.NORM_MINMAX);
			
			Mat graySign= new Mat(sroadSign.rows(), sroadSign.cols(), sroadSign.type());
			Imgproc.cvtColor(sroadSign, graySign, Imgproc.COLOR_BGRA2GRAY);
			Core.normalize(graySign, graySign, 0, 255, Core.NORM_MINMAX);
			
			//Extraction des descpteurs et keypoints
			FeatureDetector orbDetector = FeatureDetector.create(FeatureDetector.ORB);
			DescriptorExtractor orbExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
			
			MatOfKeyPoint objectKeypoints = new MatOfKeyPoint();
			orbDetector.detect(grayObject,  objectKeypoints);
			
			MatOfKeyPoint signKeypoints = new MatOfKeyPoint();
			orbDetector.detect(graySign,  signKeypoints);
			
			Mat objectDescriptor = new Mat(object.rows(), object.cols(), object.type());
			orbExtractor.compute(grayObject,  objectKeypoints,  objectDescriptor);
			
			Mat signDescriptor = new Mat(sroadSign.rows(), sroadSign.cols(), sroadSign.type());
			orbExtractor.compute(graySign,  objectKeypoints,  signDescriptor);
			
			//matching
			MatOfDMatch matchs = new MatOfDMatch();
			DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
			matcher.match(objectDescriptor,  signDescriptor, matchs);
			System.out.println(matchs.dump());
			Mat matchedImage = new Mat(sroadSign.rows(), sroadSign.cols()*2, sroadSign.type());
			Features2d.drawMatches(sObject, objectKeypoints, sroadSign, objectKeypoints, matchs, matchedImage);
			return matchedImage;
		}
		
// permet  de faire passer l'image en Noir et blanc,  les param�tres son sp�cifique pour le jeu de tableau fournit 
		public static Mat RGB2NBreference(Mat matRGB){
	        Mat matNB= new Mat();
	        Imgproc.resize(matRGB, matNB, FORMAT_STANDARD);
	        for( int i=0;i<matNB.rows();i++){
	            for( int j=0;j<matNB.cols();j++){
	                double [] rgb= matNB.get(i,j);
	                if(rgb[0]>0 && rgb[1]>0 && rgb[2]>100 ){
	                    matNB.put(i, j, new double [] {0,0,0} );
	                }
	                else{
	                    matNB.put(i, j, new double [] {255,255,255} );
	                }
	            }
	        }
	        return matNB;
	    }
		// permet  de faire passer l'image en Noir et blanc,  les param�tres son sp�cifique pour la video et les photos ( jeu de lumi�re etc... )
		public static Mat RGB2NBpanneau(Mat matRGB){
	        Mat matNB= new Mat();
	        Imgproc.resize(matRGB, matNB, FORMAT_STANDARD);
	        for( int i=0;i<matNB.rows();i++){
	            for( int j=0;j<matNB.cols();j++){
	                double [] rgb= matNB.get(i,j);
	                if(rgb[0]>2 && rgb[1]>2 && rgb[2]>90 ){
	                    matNB.put(i, j, new double [] {0,0,0} );
	                }
	                else{
	                    matNB.put(i, j, new double [] {255,255,255} );
	                }
	            }
	        }
	        return matNB;
	    }
		
		/*public static Mat superposition(Mat panneau,  Mat ref){
				Mat superposition = Mat.zeros(panneau.size(),panneau.type());
			double [] noir = new double [] {0,0,0};
				double [] blanc = new double [] {255,255,255};
				for( int i=0;i<panneau.rows();i++){
					for( int j=0;j<panneau.cols();j++){
						if(panneau.get(i, j)==noir && ref.get(i, j)==noir){
							superposition.put(i, j, new double [] {0,0,0} );
						}
						else{
							superposition.put(i, j, new double [] {255,255,255} );
						}
					}

				}

				return superposition;
				Core.bitwise_and(panneau, ref, superposition);
			}*/
		
		// on compte le nombre de pixel blanc sur une image
			public static int compte(Mat panneau, Mat ref){
				Mat superposition =new Mat();
				int compteur=0;
				Core.bitwise_and(panneau, ref, superposition);
				double [] noir = new double [] {0,0,0};
				double [] blanc = new double [] {255,255,255};
				for( int i=0;i<superposition.rows();i++){
					for( int j=0;j<superposition.cols();j++){
						if(superposition.get(i, j)[1]>10){
							compteur =compteur+1;
						}

					}

				}
				
				return compteur;
				
			}
			
			// permet de bitwise 2 panneau puis de juger quel ref est la plus proche du panneau initial.
			public static int jugement(Mat panneau, Mat ref110, Mat ref30, Mat ref50, Mat ref70, Mat ref90, Mat refdouble){
				Mat a110=new Mat();
				Mat a90=new Mat();
				Mat a50=new Mat();
				Mat a30=new Mat();
				Mat a70=new Mat();
				Mat adouble=new Mat();
				Core.bitwise_and(panneau, ref110, a110);
				//ImShow("AAA1", a110);
				Core.bitwise_and(panneau, ref90, a90);
				Core.bitwise_and(panneau, ref50, a50);
				Core.bitwise_and(panneau, ref30, a30);			
				Core.bitwise_and(panneau, ref70, a70);	
				Core.bitwise_and(panneau, refdouble, adouble);	
				//ImShow("AAA3", a30);
				//ImShow("AAA9", a90);
				//ImShow("AAA5", a50);
				//ImShow("AAA7", a70);
				//ImShow("AAAdouble", adouble);
				int[] tab =new int [6];
				tab[0]=compte(panneau ,a110);
				tab[1]=compte(panneau ,a90);
				tab[2]=compte(panneau ,a70);
				tab[3]=compte(panneau ,a50);
				tab[4]=compte(panneau ,a30);
				tab[5]=compte(panneau ,adouble);
				System.out.println("panneau 110  "+tab[0]);
				System.out.println("panneau 90  "+tab[1]);
				System.out.println("panneau 70  "+tab[2]);
				System.out.println("panneau 50  " +tab[3]);
				System.out.println("panneau 30  " +tab[4]);
				System.out.println("panneau double  " +tab[5]);
				int valeur=tab[0];
				int b=0;
				for (int i = 1; i < 6; i++) {
					if (valeur<tab[i]){
						valeur=tab[i];
						
					}
					else{}
					
					
				}
				if(valeur==tab[0]){
					System.out.println("Panneau 110");
					affiche =110;
				}
				if(valeur==tab[1]){
					System.out.println("Panneau 90");
					affiche =90;
				}
				if(valeur==tab[2]){
					System.out.println("Panneau 70");
					affiche =70;
				}
				if(valeur==tab[3]){
					System.out.println("Panneau 50");
					affiche =50;
				}
				if(valeur==tab[4]){
					System.out.println("Panneau 30");
					affiche =30;
				}
				if(valeur==tab[5]){
					System.out.println("Panneau interdit de doubler");
					affiche =1;
				}
				
				return b ;
			}
			//tentative permet de faire l'ensemble du traitemetn d'image jusqu'a la determination du carr� ayant le cercle inscrit trouv� sur 
			//l'image, et la transforme en format standart, elle est utilis� pour l'analyse de la base de donn�e
			public static Mat tentative (Mat m){
				Mat sObjectcolor = new Mat();
				//traitement de l'image 
				System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
				Mat hsv_image = Mat.zeros(m.size(), m.type());
				Imgproc.cvtColor(m, hsv_image, Imgproc.COLOR_BGR2HSV);
				//Utile.ImShow("HSV", hsv_image);
				Mat threshold_img=Utile.Seuillage(hsv_image);
				//Utile.ImShow("Seuillage", threshold_img);
				int thresh = 100;
				Mat canny_output = new Mat();
				List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
				
				
				MatOfInt4 hierarchy=new MatOfInt4();
				Imgproc.Canny(threshold_img, canny_output, thresh, thresh*2);
				Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				//Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
				MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
				
				
				float[] radius = new float[1];
				Point center = new Point();
				for(int c=0;c<contours.size();c++){
					MatOfPoint contour = contours.get(c);
					double contourArea = Imgproc.contourArea(contour);
					matOfPoint2f.fromList(contour.toList());
					Imgproc.minEnclosingCircle(matOfPoint2f,  center,  radius);
					if((contourArea/(Math.PI*radius[0]*radius[0]))>=0.79){
						// on detecte un cercle
						Core.circle(m, center, (int)radius[0], new Scalar(0, 255, 0), 2);
						org.opencv.core.Rect rect = Imgproc.boundingRect(contour);
						Core.rectangle(m,  new Point(rect.x,rect.y),
								new Point(rect.x+rect.width,rect.y+rect.height),
								new Scalar(0, 255, 0), 2);
						Mat tmp = m.submat(rect.y,rect.y+rect.height,rect.x,rect.x+rect.width);
						// on forme le carr�
						Mat ball = Mat.zeros(tmp.size(),tmp.type());
						tmp.copyTo(ball);
						//Utile.ImShow("Ball"+c, ball);
						Mat sroadSign = ball;
						Mat sObject = new Mat();
						// on met l'image extraite au format standard
						Imgproc.resize(ball,  sObject,  FORMAT_STANDARD);
						Imgproc.resize(ball,  sObjectcolor,  FORMAT_STANDARD);
						Mat grayObject = new Mat(sObject.rows(), sObject.cols(), sObject.type());
						Imgproc.cvtColor(sObject, grayObject, Imgproc.COLOR_BGRA2GRAY);
						Core.normalize(sObject, grayObject, 0, 255, Core.NORM_MINMAX);
						
					}
				}

				return sObjectcolor;
			}
			
		
		public static  void extraire (Mat m,Mat ref110,Mat ref30,Mat ref50,Mat ref70,Mat ref90, Mat refdouble){
			// initialisation  des panneaux de reference
			// extraire est la fonction globale , elle utilise toute les autre fonctions et prend en argument la base de donn�e + l'image a traiter
		
		

			
			
			
// analyse de l'image
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			Mat hsv_image = Mat.zeros(m.size(), m.type());
			Imgproc.cvtColor(m, hsv_image, Imgproc.COLOR_BGR2HSV);
			//Utile.ImShow("HSV", hsv_image);
			Mat threshold_img=Utile.Seuillage(hsv_image);
			//Utile.ImShow("Seuillage", threshold_img);
			int thresh = 100;
			Mat canny_output = new Mat();
			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			
			
			MatOfInt4 hierarchy=new MatOfInt4();
			Imgproc.Canny(threshold_img, canny_output, thresh, thresh*2);
			Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			Mat drawing = Mat.zeros(canny_output.size(), CvType.CV_8UC3);
			MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
			
			
			float[] radius = new float[1];
			Point center = new Point();
			for(int c=0;c<contours.size();c++){
				MatOfPoint contour = contours.get(c);
				double contourArea = Imgproc.contourArea(contour);
				matOfPoint2f.fromList(contour.toList());
				Imgproc.minEnclosingCircle(matOfPoint2f,  center,  radius);
				if((contourArea/(Math.PI*radius[0]*radius[0]))>=0.813){
					//obtention des cercle 
					Core.circle(m, center, (int)radius[0], new Scalar(0, 255, 0), 2);
					org.opencv.core.Rect rect = Imgproc.boundingRect(contour);
					Core.rectangle(m,  new Point(rect.x,rect.y),
							new Point(rect.x+rect.width,rect.y+rect.height),
							new Scalar(0, 255, 0), 2);
					Mat tmp = m.submat(rect.y,rect.y+rect.height,rect.x,rect.x+rect.width);
					//cr�ation des carr�es
					Mat ball = Mat.zeros(tmp.size(),tmp.type());
					tmp.copyTo(ball);
					//Utile.ImShow("Ball"+c, ball);
					Mat sroadSign = ball;
					Mat sObject = new Mat();
					Mat sObjectcolor = new Mat();
					// on met au format standard
					Imgproc.resize(ball,  sObject,  FORMAT_STANDARD);
					Imgproc.resize(ball,  sObjectcolor,  FORMAT_STANDARD);
					Mat grayObject = new Mat(sObject.rows(), sObject.cols(), sObject.type());
					Imgproc.cvtColor(sObject, grayObject, Imgproc.COLOR_BGRA2GRAY);
					Core.normalize(sObject, grayObject, 0, 255, Core.NORM_MINMAX);
					// differente �tape d'analyse qui ont �t� utile a l'�laboration du logiciel
					// afin de r�gler les diff�rents param�tre
					Mat graySign= new Mat(sObject.rows(), sObject.cols(), sObject.type());
					Imgproc.cvtColor(sObject, graySign, Imgproc.COLOR_BGRA2GRAY);
					Core.normalize(graySign, graySign, 0, 255, Core.NORM_MINMAX);
					//Utile.ImShow("Ball12"+c, graySign);
					
					Mat hsv_image1 = Mat.zeros(sObject.size(), sObject.type());
					Imgproc.cvtColor(sObject, hsv_image1, Imgproc.COLOR_BGR2HSV);
					//Utile.ImShow("HSV", hsv_image1);
					Mat threshold_img1=Utile.Seuillage(hsv_image1);
					//Utile.ImShow("Seuillage", threshold_img1);

					Mat canny_output1 = new Mat();
					List<MatOfPoint> contours1 = new ArrayList<MatOfPoint>();
					
					MatOfInt4 hierarchy1=new MatOfInt4();
					Imgproc.Canny(threshold_img1, canny_output1, thresh, thresh*2);
					Imgproc.findContours(canny_output1, contours1, hierarchy1, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
					Mat drawing1 = Mat.zeros(canny_output1.size(), CvType.CV_8UC3);
					Mat matrice3 =new Mat();
					// on transforme le panneau obtenue en noir et blanc
					matrice3=RGB2NBpanneau(ball);
					//ImShow("test1", matrice3);
					int resultat;
					// on compart le panneau grace a a fonction jugement
					resultat=jugement(matrice3, ref110, ref30, ref50, ref70, ref90, refdouble);
					//ImShow("110", ref110);
					//ImShow("90", ref90);
					//ImShow("70", ref70);
					//ImShow("30", ref30);
					//ImShow("double", refdouble);

					


					
					//System.out.println(resultat);
					/*int compteur=0;
					Mat matriceutile=sObjectcolor;
					System.out.println(matriceutile);
					double [] travail =new double[3];
					for (int i = 0; i < 255; i++) {
						for (int j = 0; j < 255; j++) {
							if (matriceutile.get(i,j)[0]>2&&matriceutile.get(i,j)[1]>2&&matriceutile.get(i,j)[2]>2) {
							
							System.out.println("yo");
							}*/
							
								
								
								
							
							
									
						}	
					}
				
					

				}
			}