package test_opencv;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.EventQueue;
import java.awt.Font;

public class Fenetre extends JFrame implements Runnable {
	static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
	// imag va prendre la vid�o image par image ensuite l'afficher
	static Mat imag = null;
	private JPanel contentPane;
	// le thread permet de lancer la vid�o en cliquant sur un bouton
	private Thread T1;
	//lancer sert � d�terminer si c'est la cam�ra ou la vid�o qu'on lance
	static int lancer=0;
	//si help et imp�raire le bouton help fait apparaitre l'aide au deuxi�me clique la fen�tre disparait
	public int help=0;
	// en r�cupr�rant la  valeur de la fonction jugement 
	//cette variable affiche le panneau de r�f�rence en bas de la fenetre lors de l'analyse de la vid�o
	public static int affiche=0;
	

	/**
	 * Create the frame.
	 */
	public Fenetre() {
		//fenetre globale et ses param�tres
		setTitle("VampsDetec : D�tecteur de panneaux");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 964, 790);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//button lancement de la vid�o
		JButton btnNewButton = new JButton("Lancer la vid\u00E9o");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lancer=1;
				start();
			}
		});
		//fixation du placement du button
		btnNewButton.setBounds(135, 529, 151, 42);
		//ajout du button au panel
		contentPane.add(btnNewButton);
		//button pour lancer la cam�ra
		JButton btnNewButton_1 = new JButton("Lancer la cam\u00E9ra");
		btnNewButton_1.setBounds(595, 529, 151, 42);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lancer=0;
				start();
			}
		});
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(135, 99, 596, 385);
		lblNewLabel.setIcon(new ImageIcon("twizy.png"));
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Bienvenue dans VampsDetect");
		lblNewLabel_1.setFont(new Font("Verdana", Font.PLAIN, 24));
		lblNewLabel_1.setBounds(228, 36, 380, 62);
		contentPane.add(lblNewLabel_1);
		
		//button aide
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setBounds(783, 13, 121, 98);
		btnNewButton_2.setIcon(new ImageIcon("I.png"));
		contentPane.add(btnNewButton_2);
		
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Fenetre2 frame = new Fenetre2();
					btnNewButton_2.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							help++;
							if(help%2==0){
								frame.setVisible(false);
							}
							else{
								frame.setVisible(true);
							}
							
						}
					});
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		
	}
	//impl�mentation du thread
	 public void start(){
		  T1 = new Thread(this);
		  T1.start();
	  }

	@Override
	public void run() {
		// le lavel qui va affihcer le panneau de r�f�rence
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(312, 487, 256, 256);
		contentPane.add(lblNewLabel_2);
		// initialisation des variable de la classe utile 
		Mat ref110 = new Mat();
		Mat ref30 = new Mat();
		Mat ref50 = new Mat();
		Mat ref70= new Mat();
		Mat ref90 = new Mat();
		Mat refdouble = new Mat();
		// imporatation de la base de donn�e
		ref110=Utile.LectureImage("ref110.jpg");
		ref30=Utile.LectureImage("ref3.jpg");
		ref50=Utile.LectureImage("ref50.jpg");
		ref70=Utile.LectureImage("ref70.jpg");
		ref90=Utile.LectureImage("ref90.jpg");
		refdouble=Utile.LectureImage("defdouble2.jpg");

		ref110=Utile.RGB2NBreference(Utile.tentative(ref110));
		ref30=Utile.RGB2NBreference(Utile.tentative(ref30));
		ref50=Utile.RGB2NBreference(Utile.tentative(ref50));
		ref70=Utile.RGB2NBreference(Utile.tentative(ref70));
		ref90=Utile.RGB2NBreference(Utile.tentative(ref90));
		refdouble=Utile.RGB2NBreference(Utile.tentative(refdouble));
		// configuration de la frame qui affichera la vid�o
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);    
		JFrame jframe = new JFrame("Vid�o test");
	    jframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    JLabel vidpanel = new JLabel();
	    jframe.setContentPane(vidpanel);
	    jframe.setSize(640, 480);
	    jframe.setLocation(900,10);
	    jframe.setVisible(true);
	    //lecture de la vid�o image par image
	    Mat frame = new Mat();
	    Size sz = new Size(640, 480);
	    VideoCapture camera;
	    //selon le bouton cliqu� (cam�ra ou vid�o )
	    //la variable lancer prend la valeur correspondante
	    //afin de lancer le bon code
	    // le principe est le meme poru les deux
	    // lire image par image
	    if(lancer==1){
	     camera= new VideoCapture("video1.avi");
	     jframe.setTitle("vid�o en cours de traitement");
	    }
	    else {
	    	camera = new VideoCapture(0);
	    	jframe.setTitle("camera activ�e");
	    }
	    //affichage d'image par imgage
	    int i=1;
	    while (true) {
	    	if (camera.read(frame)) {
	    		// la variable i comptent les frames affich�es une par une pour anylser qu'une seule sur deux
	    		i++;
	    		if((i%2)==0){
	    			//la fonction extraite fait l'analyse de la frame
	    			Mat hsv_image= Mat.zeros(frame.size(), frame.type());
	    			Mat threshold_img=Utile.Seuillage(hsv_image);
	    			Utile.extraire(frame, ref110,ref30,ref50,ref70,ref90,refdouble);
	    			//la focntion jugement donne � affihce la valeur du panneau de r�f�rence correspondant
	    			//et la suite affiche dans le label 2 l'image du panneau de r�f correspondant
	        		if(affiche==110){
	        			lblNewLabel_2.setIcon(new ImageIcon("ref110.jpg"));
	        		}
	        		else if(affiche==90){
	        			lblNewLabel_2.setIcon(new ImageIcon("ref90.jpg"));
	        		}
	        		else if (affiche==70){
	        			lblNewLabel_2.setIcon(new ImageIcon("ref70.jpg"));
	        		}
	        		else if(affiche==50){
	        			lblNewLabel_2.setIcon(new ImageIcon("ref50.jpg"));
	        		}
	        		else if(affiche==30){
	        			lblNewLabel_2.setIcon(new ImageIcon("ref30.jpg"));
	        		}
	        		else if (affiche==1){
	        			lblNewLabel_2.setIcon(new ImageIcon("refdouble.jpg"));
	        		}
	    		}	
	    		//pour afficher l'image suivante : nous convertissant l'image en buffered pour
	    		//repaindre la m�me frame, �a nous �vitera d'avoir des centaines de frames par vid�o
	    		Imgproc.resize(frame, frame, sz);
	    		Mat hsv_image1= Mat.zeros(frame.size(), frame.type());
	    		Mat threshold_img1=Utile.Seuillage(hsv_image1);
	    		// nous utilison la fonction contours sur origine pour afficher les contour dans la vid�o
	    		ImageIcon image1 = new ImageIcon(Traitement_video.Mat2bufferedImage(Utile.Contours_sur_origine (threshold_img1,frame)));
	    		vidpanel.setIcon(image1);
	    		vidpanel.repaint();
	    		//la condition if suivante va fermer la fenetre de la vid�o uen fois la derni�re frame analys�e
	    		if(lancer==1 && i==396 ){
	    			jframe.setVisible(false);
	    			jframe.dispose();
	    			lblNewLabel_2.setIcon(new ImageIcon("vide.jpg"));
	    		}	    		
	    	}
	    }
	}
}

